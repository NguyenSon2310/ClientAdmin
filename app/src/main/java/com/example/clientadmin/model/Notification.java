package com.example.clientadmin.model;

public class Notification {
    private int imgAvatar, imgStatus;
    private String txtStatus, txtTime;

    public Notification() {
    }

    public Notification(int imgAvatar, int imgStatus, String txtStatus, String txtTime) {
        this.imgAvatar = imgAvatar;
        this.imgStatus = imgStatus;
        this.txtStatus = txtStatus;
        this.txtTime = txtTime;
    }

    public int getImgAvatar() {
        return imgAvatar;
    }

    public void setImgAvatar(int imgAvatar) {
        this.imgAvatar = imgAvatar;
    }

    public int getImgStatus() {
        return imgStatus;
    }

    public void setImgStatus(int imgStatus) {
        this.imgStatus = imgStatus;
    }

    public String getTxtStatus() {
        return txtStatus;
    }

    public void setTxtStatus(String txtStatus) {
        this.txtStatus = txtStatus;
    }

    public String getTxtTime() {
        return txtTime;
    }

    public void setTxtTime(String txtTime) {
        this.txtTime = txtTime;
    }
}
