package com.example.clientadmin.model;

public class Stadium {
    private String nameStadium;
    private int icon;

    public Stadium() {
    }

    public Stadium(String nameStadium, int icon) {
        this.nameStadium = nameStadium;
        this.icon = icon;
    }

    public String getNameStadium() {
        return nameStadium;
    }

    public void setNameStadium(String nameStadium) {
        this.nameStadium = nameStadium;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
