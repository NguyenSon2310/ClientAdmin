package com.example.clientadmin.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.clientadmin.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddStadiumFragment extends Fragment {

    EditText edtNameStadium, edtPrice;
    Button btnAdd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_add_stadium, container, false);
        edtNameStadium = (EditText) row.findViewById(R.id.edt_nameStadium);
        edtPrice = (EditText) row.findViewById(R.id.edt_price);
        btnAdd = (Button) row.findViewById(R.id.btn_add);
        return row;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEvent();
    }

    private void setEvent() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ManageStadiumFragment manageStadiumFragment = new ManageStadiumFragment();
                Bundle bundle = new Bundle();
                bundle.putString("name", edtNameStadium.getText().toString());
                manageStadiumFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, manageStadiumFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
}
