package com.example.clientadmin.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.example.clientadmin.R;
import com.example.clientadmin.adapter.StadiumGridViewAdapter;
import com.example.clientadmin.model.Stadium;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManageStadiumFragment extends Fragment {

    GridView gridView;
    ArrayList<Stadium> data = new ArrayList<>();
    StadiumGridViewAdapter adapter;
    FloatingActionButton fab;
    LinearLayout lnPay, lnEdit;
    static int position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_manage_stadium, container, false);
        gridView = (GridView) row.findViewById(R.id.gridView);
        fab = (FloatingActionButton) row.findViewById(R.id.fab);
        lnPay = (LinearLayout) row.findViewById(R.id.ln_pay);
        lnEdit = (LinearLayout) row.findViewById(R.id.ln_edit);
        return row;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEvent();
    }

    private void setEvent() {
        final Bundle bundle = getArguments();
        adapter = new StadiumGridViewAdapter(getContext(), R.layout.gridview_item_row, data);
        data.clear();
        data.add(new Stadium("Sân 1", R.drawable.ball));
        data.add(new Stadium("Sân 2", R.drawable.ball));
        data.add(new Stadium("Sân 3", R.drawable.ball));
        data.add(new Stadium("Sân 4", R.drawable.ball));
        data.add(new Stadium("Sân 5", R.drawable.ball));
        if (bundle != null) {
            data.add(new Stadium(bundle.getString("name"), R.drawable.ball));
        }
        adapter.notifyDataSetChanged();
        gridView.setAdapter(adapter);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddStadiumFragment addStadiumFragment = new AddStadiumFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, addStadiumFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                position = i;
            }
        });

        lnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle1 = new Bundle();
                bundle1.putString("name", data.get(position).getNameStadium());
                ConfigInfoStadiumFragment configInfoStadiumFragment = new ConfigInfoStadiumFragment();
                configInfoStadiumFragment.setArguments(bundle1);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, configInfoStadiumFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        lnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle1 = new Bundle();
                bundle1.putString("name", data.get(position).getNameStadium());
                PaymentStadiumFragment paymentStadiumFragment = new PaymentStadiumFragment();
                paymentStadiumFragment.setArguments(bundle1);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, paymentStadiumFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
}
