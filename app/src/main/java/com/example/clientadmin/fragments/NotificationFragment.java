package com.example.clientadmin.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clientadmin.R;
import com.example.clientadmin.adapter.NotificationAdapter;
import com.example.clientadmin.model.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    RecyclerView mRecyclerView;
    NotificationAdapter adapter;
    List<Notification> data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row = inflater.inflate(R.layout.fragment_notification, container, false);
        mRecyclerView = (RecyclerView) row.findViewById(R.id.recyclerView);
        return row;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showRecycler();
    }

    @SuppressLint("WrongConstant")
    private void showRecycler() {
        data = new ArrayList<>();
        data.add(new Notification(R.drawable.addimage, R.drawable.ic_account_circle_black_24dp, "Thắng Nguyễn đã đặt sân 14h-15h30 ngày 31/10/2016", "4 phút"));
        data.add(new Notification(R.drawable.addimage, R.drawable.ic_account_circle_black_24dp, "Thắng Nguyễn đã đặt sân 14h-15h30 ngày 31/10/2016", "4 phút"));
        data.add(new Notification(R.drawable.addimage, R.drawable.ic_account_circle_black_24dp, "Thắng Nguyễn đã đặt sân 14h-15h30 ngày 31/10/2016", "4 phút"));
        adapter = new NotificationAdapter(data);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(adapter);
    }
}
