package com.example.clientadmin.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.clientadmin.R;
import com.example.clientadmin.fragments.NotifyConfirmFragment;
import com.example.clientadmin.model.Notification;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {

    private List<Notification> data = new ArrayList<>();

    public NotificationAdapter(List<Notification> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.notification_item_row, parent, false);
        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class NotificationHolder extends RecyclerView.ViewHolder {
        TextView txtStatus, txtTime;
        ImageView imgAvatar, imgStatus;
        LinearLayout line;

        public NotificationHolder(@NonNull View itemView) {
            super(itemView);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            imgAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
            imgStatus = (ImageView) itemView.findViewById(R.id.imgStatus);
            line = (LinearLayout) itemView.findViewById(R.id.line);
        }

        public void bind(Notification notification) {
            txtStatus.setText(notification.getTxtStatus());
            txtTime.setText(notification.getTxtTime());
            imgStatus.setImageResource(notification.getImgStatus());
            imgAvatar.setImageResource(notification.getImgAvatar());
            line.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    NotifyConfirmFragment myFragment = new NotifyConfirmFragment();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, myFragment).addToBackStack(null).commit();
                }
            });
        }
    }

    public interface OnItemClickedListener {
        void onItemClick();
    }

    private OnItemClickedListener onItemClickedListener;

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }
}
