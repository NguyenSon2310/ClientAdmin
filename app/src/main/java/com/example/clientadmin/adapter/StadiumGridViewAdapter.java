package com.example.clientadmin.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.clientadmin.R;
import com.example.clientadmin.model.Stadium;

import java.util.ArrayList;
import java.util.List;

public class StadiumGridViewAdapter extends ArrayAdapter<Stadium> {

    private Context context;
    private int layoutResource;
    private ArrayList<Stadium> data = null;

    public StadiumGridViewAdapter(Context context, int layoutResource, ArrayList<Stadium> data) {
        super(context, layoutResource, data);
        this.context = context;
        this.layoutResource = layoutResource;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        StadiumHolder holder = null;
        if(row == null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResource, parent, false);

            holder = new StadiumHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtName = (TextView)row.findViewById(R.id.txtName);

            row.setTag(holder);
        }else {
            holder = (StadiumHolder)row.getTag();
        }

        Stadium stadium = data.get(position);
        holder.txtName.setText(stadium.getNameStadium());
        holder.imgIcon.setImageResource(stadium.getIcon());

        return row;
    }

    static class StadiumHolder {
        TextView txtName;
        ImageView imgIcon;
    }
}
